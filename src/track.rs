use std::collections::BTreeSet;
use crate::remark::{Remark, Remarks, RemarkType};
use crate::util::{collect_texts, maybe_write};
use crate::flag::Flag;
use crate::token::{CueToken,CueSymbol};
use crate::cue_point::{CuePoint,CuePointParseError};

/// A track index.
///
/// Represented as "INDEX <index> <point: MM:SS:FF>" in cuesheets.
///
/// Lists the track's offset from the beginning of the cue.
#[derive(Debug,Default,Clone,PartialEq,Eq)]
pub struct Index {
    pub index: usize,
    pub point: CuePoint,
}
impl<'a> Index {
    pub fn new(index: usize, point: CuePoint) -> Self {
        Self{ index, point }
    }
    pub fn from_cue(index: usize, point_str: &'a str) -> Result<Index, CuePointParseError> {
        Ok(Index {
            index,
            point: CuePoint::from_point_str(point_str)?,
        })
    }

    pub fn try_from_duration(index: usize, duration: std::time::Duration) -> Result<Self, std::num::TryFromIntError> {
        Ok(Index {
            index,
            point: CuePoint::try_from_duration(duration)?,
        })
    }
}

/// prints the index in its original cue sheet format
impl std::fmt::Display for Index {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "INDEX {:02} {}", self.index, self.point)
    }
}

#[cfg(test)]
mod test_index {
    use super::Index;
    #[test]
    fn display() {
        assert_eq!("INDEX 02 44:12:37".to_string(), Index::from_cue(2, "44:12:37").unwrap_or_else(|_| Default::default()).to_string());
    }
}

/// Recognized track types.
///
/// Note that miscellaneous binary data types are not recognized by cuet.
#[derive(Debug,Default,Clone,PartialEq,Eq)]
pub enum TrackType {
    Audio,
    Data,
    Miscellaneous(String),
    #[default]
    Unknown,
}

impl From<CueToken> for TrackType {
    fn from(token: CueToken) -> Self {
        match token {
            CueToken::TrackAudio => Self::Audio,
            CueToken::TrackData => Self::Data,
            _ => Self::Unknown,
        }
    }
}

impl std::fmt::Display for TrackType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use TrackType::*;
        match self {
            Audio => write!(f, "AUDIO"),
            Data => write!(f, "DATA"),
            Miscellaneous(string) => write!(f, "{}", string),
            _ => write!(f, "UKNOWN"),
        }
    }
}


/// Single track in cue sheet.
#[derive(Debug,Default,Clone,PartialEq,Eq)]
pub struct Track {
    pub track_type: TrackType,
    pub flags: BTreeSet<Flag>,
    /// track time indices
    ///
    /// A track may have more than one index. If so the first index is the start of its
    /// pregap, and the second is the start of the audio.
    pub indices: Vec<Index>,
    /// track's unique identifier (see:
    /// <https://en.wikipedia.org/wiki/International_Standard_Recording_Code>)
    pub isrc: Option<String>,
    /// track artist
    pub performer: Option<String>,
    /// time of silence after track
    pub postgap: Option<CuePoint>,
    /// time of silence before track
    pub pregap: Option<CuePoint>,
    /// any miscellaneous comments
    pub remarks: Remarks,
    pub songwriter: Option<String>,
    pub title: Option<String>,
    pub number: usize,
}

impl std::fmt::Display for Track {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "TRACK {:02} {}", self.number, self.track_type)?;
        maybe_write(f, "  TITLE", &self.title)?;
        maybe_write(f, "  PERFORMER", &self.performer)?;
        maybe_write(f, "  SONGWRITER", &self.songwriter)?;
        if let Some(v) = self.pregap.as_ref() { writeln!(f, "  PREGAP {}", v)?; };
        if let Some(v) = self.postgap.as_ref() { writeln!(f, "  POSTGAP {}", v)?; };
        if let Some(v) = self.isrc.as_ref() { if !v.is_empty() { writeln!(f, "  ISRC {}", v)?; } };
        if !self.flags.is_empty() {
            write!(f, "  FLAGS ")?;
            let flags = self.flags.iter().map(|f| f.to_string()).collect::<Vec<String>>().join(", ");
            writeln!(f, "{}", flags)?;
        }
        for remark in &self.remarks { remark.maybe_write(f, "  ")?; }
        for index in &self.indices { writeln!(f, "  {}", index)?; }
        Ok(())
    }
}

const UNKNOWN: &str = "UNKNOWN";

impl Track {
    /// Create a track of a given type.
    pub fn new(track_type: TrackType) -> Self {
        Self{ track_type, ..Default::default() }
    }

    /// convenience method for adding remarks
    pub fn append_remark(&mut self, remark_type: RemarkType, text: String) {
        self.remarks.push(Remark::new(remark_type, text));
    }

    /// convenience method for adding indices from "HH:MM:FF" string
    pub fn append_index_str(&mut self, index: usize, cue_point: &str) {
        if let Ok(index) = Index::from_cue(index, cue_point) {
            self.indices.push(index);
        }
    }

    /// convenience method for adding indices from durations
    pub fn append_index_duration(&mut self, index: usize, cue_point: &str) {
        if let Ok(index) = Index::from_cue(index, cue_point) {
            self.indices.push(index);
        }
    }

    pub fn get_title(&self) -> &str {
        match &self.title {
            Some(string) => string.as_ref(),
            None => UNKNOWN,
        }
    }
}

#[derive(Default,Debug,Clone,PartialEq,Eq)]
pub struct TrackParser {
    track: Track,
}

impl TrackParser {
    pub fn from_line(line: &[CueSymbol]) -> Option<TrackParser> {
        use CueSymbol::{Command,Number};
        use CueToken as T;
        match line {
            [Command(T::Track), Number(n), CueSymbol::TrackType(track_type)] => Some(Self {
                track: Track {
                    track_type: track_type.clone(),
                    number: *n,
                    ..Default::default()
                }
            }),
            [Command(T::Track), Number(n), CueSymbol::Word(s)] => Some(Self {
                track: Track {
                    track_type: TrackType::Miscellaneous(s.clone()),
                    number: *n,
                    ..Default::default()
                }
            }),
            _ => None,
        }
    }

    pub fn take_line(&mut self, line: &[CueSymbol]) -> bool {
        use CueSymbol::{Command,StringLiteral,Number};
        use CueToken as T;
        match &line {
            [Command(T::Flags), ..] => {
                line[1..].iter().for_each(|s| {
                   if let  CueSymbol::Flag(f) = s {
                       self.track.flags.insert(*f);
                   };
                });
                true
            },
            [Command(T::Isrc), ..] => {
                self.track.isrc = Some(collect_texts(&line[1..]));
                true
            },
            [Command(T::Performer), ..] => {
                self.track.performer = Some(collect_texts(&line[1..]));
                true
            }
            [Command(T::Postgap), StringLiteral(cue_point)] => {
                self.track.postgap = CuePoint::from_point_str(cue_point).ok();
                true
            },
            [Command(T::Pregap), StringLiteral(cue_point)] => {
                self.track.pregap = CuePoint::from_point_str(cue_point).ok();
                true
            },
            [Command(T::Rem), ..] => {
                self.track.remarks.push(Remark::from_line(line));
                true
            },
            [Command(T::Songwriter), ..] => {
                self.track.songwriter = Some(collect_texts(&line[1..]));
                true
            }
            [Command(T::Title), ..] => {
                self.track.title = Some(collect_texts(&line[1..]));
                true
            }
            [Command(T::Index), Number(n), StringLiteral(c)] => {
                if let Ok(index) = Index::from_cue(*n, c) {
                    self.track.indices.push(index);
                }
                true
            },
            _ => false,

        }
    }

    pub fn finish(self) -> Track {
        self.track
    }
}

#[cfg(test)]
mod test_track_parser {
    use super::{CueToken, CuePoint, Index, TrackParser, Track, TrackType, Flag};
    const TEST_TEXT: &str = r#"   TRACK 13 AUDIO
    TITLE An "Amazing" Song
    PERFORMER "My Cool Band"
    FLAGS DCP, 4CH
    SONGWRITER Some Other Guy, PhD
    REM COMMENT ""
    PREGAP 00:03:00
    POSTGAP 00:01:00
    ISRC ABCDE12345
    INDEX 00 54:11:00
    INDEX 01 54:12:40"#;
    const CLEAN_TEXT: &str = r#"TRACK 13 AUDIO
  TITLE "An \"Amazing\" Song"
  PERFORMER "My Cool Band"
  SONGWRITER "Some Other Guy, PhD"
  PREGAP 00:03:00
  POSTGAP 00:01:00
  ISRC ABCDE12345
  FLAGS DCP, SCMS
  REM DATE "2022"
  REM GENRE "Folk"
  REM DISCID "ABCDE12345"
  INDEX 00 54:11:00
  INDEX 01 54:12:40
"#;

    fn do_parse(text: &str) -> Track {
        let mut lines = text.lines();
        let mut parser: Option<TrackParser> = TrackParser::from_line(&CueToken::parse_line(lines.next().unwrap())[..]);
        assert!(parser.is_some());
        for line in lines {
            parser.as_mut().unwrap().take_line(&CueToken::parse_line(line)[..]);
        }
        parser.unwrap().finish()
    }

    #[test]
    fn from_line() {
        let line = CueToken::parse_line("TRACK 01 AUDIO");
        assert_eq!(TrackParser::from_line(&line).unwrap().finish(), Track{
            track_type: TrackType::Audio,
            number: 1,
            ..Default::default()
        });
        let bin_line = CueToken::parse_line("TRACK 10 MODE1/2352");
        assert_eq!(TrackParser::from_line(&bin_line).unwrap().finish(), Track{
            track_type: TrackType::Miscellaneous("MODE1/2352".into()),
            number: 10,
            ..Default::default()
        });
    }

    #[test]
    fn parse() {
        let track = do_parse(TEST_TEXT);
        assert_eq!(track.number, 13);
        assert_eq!(track.track_type, TrackType::Audio);
        assert_eq!(track.performer, Some("My Cool Band".into()));
        assert_eq!(track.songwriter, Some("Some Other Guy, PhD".into()));
        assert_eq!(track.flags, std::collections::BTreeSet::from([Flag::Dcp, Flag::FourCh]));
        assert_eq!(track.title, Some(r#"An "Amazing" Song"#.into()));
        assert_eq!(track.pregap, Some(CuePoint{ minute: 0, second: 3, frame: 0 }));
        assert_eq!(track.postgap, Some(CuePoint{ minute: 0, second: 1, frame: 0 }));
        assert_eq!(track.isrc, Some("ABCDE12345".into()));
        assert_eq!(track.indices, vec![
                   Index{ index: 0, point: CuePoint{ minute: 54, second: 11, frame: 0 } },
                   Index{ index: 1, point: CuePoint{ minute: 54, second: 12, frame: 40 } },
        ]);
    }

    #[test]
    fn display() {
        let track = do_parse(CLEAN_TEXT);
        assert_eq!(CLEAN_TEXT, track.to_string());
    }
}
