use crate::token::CueToken;

/// Track flags.
#[derive(Debug,Default,Copy,Clone,PartialEq,Eq,PartialOrd,Ord)]
pub enum Flag {
    /// Digial Copy Permitted
    Dcp,
    /// Four channel audio
    FourCh,
    /// Pre-emphasis enabled
    Pre,
    /// Serial Copy Management System
    Scms,
    /// Unrecognized flag type
    #[default]
    Unknown,
}

impl From<CueToken> for Flag {
    /// converts a parsed token to a Flag.
    fn from(token: CueToken) -> Self {
        match token {
            CueToken::FlagDcp => Self::Dcp,
            CueToken::FlagFourCh => Self::FourCh,
            CueToken::FlagPre => Self::Pre,
            CueToken::FlagScms => Self::Scms,
            _ => Self::Unknown,
        }
    }
}

impl std::fmt::Display for Flag {
    /// Displays a Flag as it originally appeared.
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Dcp => write!(f, "DCP"),
            Self::FourCh => write!(f, "4CH"),
            Self::Pre => write!(f, "PRE"),
            Self::Scms => write!(f, "SCMS"),
            _ => write!(f, "UNKNOWN"),
        }
    }
}
