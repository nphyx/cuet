use logos::Logos;
use crate::util::dequote;
use crate::track::TrackType;
use crate::file::FileType;
use crate::flag::Flag;
use crate::remark::RemarkType;

#[derive(Logos,Copy,Clone,Debug,PartialEq,Eq)]
pub enum CueToken {
    // Commands
    #[token("CATALOG")]
    Catalog,
    #[token("CDTEXTFILE")]
    CdTextFile,
    #[token("FILE")]
    File,
    #[token("FLAGS")]
    Flags,
    #[regex("DCP,?")]
    FlagDcp,
    #[regex("4CH,?")]
    FlagFourCh,
    #[regex("PRE,?")]
    FlagPre,
    #[regex("SCMS,?")]
    FlagScms,
    #[token("INDEX")]
    Index,
    #[token("ISRC")]
    Isrc,
    #[token("PERFORMER")]
    Performer,
    #[token("POSTGAP")]
    Postgap,
    #[token("PREGAP")]
    Pregap,
    #[token("REM")]
    Rem,
    #[token("SONGWRITER")]
    Songwriter,
    #[token("TITLE")]
    Title,
    #[token("TRACK")]
    Track,
    // Known Remark Types
    #[token("GENRE")]
    RemarkGenre,
    #[token("DATE")]
    RemarkDate,
    #[token("DISCID")]
    RemarkDiscId,
    #[token("COMMENT")]
    RemarkComment,
    // File Types
    #[token("WAVE")]
    FileWave,
    #[token("MP3")]
    FileMp3,
    #[token("AIFF")]
    FileAiff,
    #[token("BINARY")]
    FileBinary,
    #[token("MOTOROLA")]
    FileMotorola,
    // Track Types
    #[token("AUDIO")]
    TrackAudio,
    #[token("DATA")]
    TrackData,
    // Miscellany
    #[regex("[\t ]+", logos::skip)]
    Whitespace,
    #[regex(r"\d+")]
    Numeric,
    #[regex(r#""([^"]|[\\]["])+""#)]
    QuotedString,
    #[regex(r#""""#)]
    EmptyString,
    #[regex("[^ ]+", priority = 0)]
    Text,
    #[regex(r"\d+:\d+:\d+", priority = 1)] // should be r"\d{2}:\d{2}:\d{2}" but this isn't supported
    CuePoint,
    #[error]
    Error
}

#[derive(Debug,PartialEq,Eq)]
pub enum CueSymbol {
    Command(CueToken),
    StringLiteral(String),
    Word(String),
    Number(usize),
    FileType(FileType),
    TrackType(TrackType),
    RemarkType(RemarkType),
    Flag(Flag),
}

impl CueSymbol {
    pub fn word(string: &str) -> Self {
        Self::Word(string.into())
    }

    pub fn string(string: String) -> Self {
        Self::StringLiteral(dequote(string))
    }
}

type CueLine = Vec<CueSymbol>;

impl<'a> CueToken {
    pub fn parse_line(line: &'a str) -> CueLine {
        use CueToken::*;
        let mut lex = CueToken::lexer(line);
        let mut items: CueLine = CueLine::new();
        while let Some(item) = lex.next() {
            match item {
                EmptyString => {},
                QuotedString => items.push(CueSymbol::string(lex.slice().into())),
                Text => items.push(CueSymbol::word(lex.slice())),
                Numeric => {
                    let strnum = lex.slice().to_string();
                    let num = strnum.parse::<usize>().unwrap();
                    items.push(CueSymbol::Number(num));
                }
                CuePoint => {
                    items.push(CueSymbol::string(lex.slice().into()));
                }
                RemarkDate | RemarkDiscId | RemarkGenre | RemarkComment => {
                    items.push(CueSymbol::RemarkType(RemarkType::from(item)));
                }
                FileWave | FileMp3 | FileAiff | FileBinary | FileMotorola => {
                    items.push(CueSymbol::FileType(FileType::from(item)));
                }
                TrackAudio | TrackData => {
                    items.push(CueSymbol::TrackType(TrackType::from(item)));
                }
                FlagDcp | FlagFourCh | FlagPre | FlagScms => {
                    items.push(CueSymbol::Flag(Flag::from(item)));
                }
                token => items.push(CueSymbol::Command(token)),
            }
        }
        items
    }
}

#[cfg(test)]
mod test_cue_token {
    use super::{CueToken, CueSymbol as S, CueToken::*, CueSymbol::*};
    use crate::remark;

    #[test]
    pub fn parse_line() {
        let parsed_catalog = CueToken::parse_line("CATALOG 123456");
        assert_eq!(parsed_catalog.as_slice(), [Command(Catalog), S::Number(123456)]);
        let parsed_cdtextfile = CueToken::parse_line(r#"CDTEXTFILE C:\Users\Bob\My Documents\my_cd.txt"#);

        assert_eq!(parsed_cdtextfile, [Command(CdTextFile), S::word(r#"C:\Users\Bob\My"#), S::word(r#"Documents\my_cd.txt"#)]);

        let parsed_performer = CueToken::parse_line("PERFORMER My Cool Band");
        assert_eq!(parsed_performer, [Command(Performer), S::word("My"), S::word("Cool"), S::word("Band")]);

        let parsed_comment_quoted = CueToken::parse_line(r#"REM COMMENT "This is a comment""#);
        assert_eq!(parsed_comment_quoted, [Command(Rem), RemarkType(remark::RemarkType::Comment), S::string("This is a comment".into())]);

        let parsed_title = CueToken::parse_line(r#"TITLE An "Amazing" Song"#);
        assert_eq!(parsed_title, [Command(Title), S::word("An"), S::string("Amazing".into()), S::word("Song")]);

        let parsed_escaped_quotes = CueToken::parse_line(r#"TITLE "An \"Amazing\" Song""#);
        assert_eq!(parsed_escaped_quotes, [Command(Title), S::string(r#"An \"Amazing\" Song"#.into())]);
    }
}
