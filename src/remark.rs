use crate::util::collect_texts;
use crate::token::{CueSymbol,CueToken};

/// Recognized remark types.
///
/// Unrecognized types will be parsed as `RemarkType::Miscellaneous`.
/// If they have a non-standard subcommand (e.g. `REPLAY-GAIN`), it will be prepended to the
/// remark's text.
#[derive(Default,Debug,Clone,PartialEq,Eq)]
pub enum RemarkType {
    Genre,
    Date,
    DiscId,
    Comment,
    Miscellaneous(String),
    #[default]
    Unknown,
}

impl From<CueToken> for RemarkType {
    fn from(token: CueToken) -> RemarkType {
        use CueToken::*;
        use RemarkType::*;
        match token {
            RemarkGenre => Genre,
            RemarkDate => Date,
            RemarkDiscId => DiscId,
            RemarkComment => Comment,
            _ => Unknown,
        }
    }
}

impl std::fmt::Display for RemarkType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Genre => write!(f, "GENRE"),
            Self::Date => write!(f, "DATE"),
            Self::DiscId => write!(f, "DISCID"),
            Self::Comment => write!(f, "COMMENT"),
            Self::Miscellaneous(s) => write!(f, "{}", s),
            Self::Unknown => write!(f, "UNKNOWN"),
        }
    }
}


/// A remark entry.
#[derive(Default,Debug,Clone,PartialEq,Eq)]
pub struct Remark {
    pub remark_type: RemarkType,
    pub text: String,
}

impl Remark {
    pub fn from_line(line: &[CueSymbol]) -> Remark {
        use CueToken as T;
        use CueSymbol as S;
        match line {
            [S::Command(T::Rem), S::RemarkType(t), ..] => {
                Self {
                    remark_type: t.clone(),
                    text: collect_texts(&line[2..]),
                }
            }
            [S::Command(T::Rem), S::Word(w), ..] => {
                Self {
                    remark_type: RemarkType::Miscellaneous(w.clone()),
                    text: collect_texts(&line[2..]),
                }
            }
            _=> { // shouldn't happen but whatever, let's not make a thing of it
                Self {
                    remark_type: RemarkType::Unknown,
                    text: collect_texts(line),
                }
            }
        }
    }
}

impl std::fmt::Display for Remark {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.remark_type {
            RemarkType::Miscellaneous(s) => write!(f, "REM {} \"{}\"", s, self.text),
            _ => write!(f, "REM {} \"{}\"", self.remark_type, self.text),
        }
    }
}

impl Remark {
    pub fn new(remark_type: RemarkType, text: String) -> Self {
        Remark{ remark_type, text }
    }
    /// Writes itself to the formatter only if its text isn't empty.
    ///
    /// For use with Track::fmt and CueSheet::fmt.
    pub fn maybe_write(&self, f: &mut std::fmt::Formatter<'_>, indent: &str) -> std::fmt::Result {
        if !self.text.is_empty() {
            writeln!(f, "{}{}", indent, self)?;
        }
        Ok(())
    }
}

pub type Remarks = Vec<Remark>;

#[cfg(test)]
mod test_remark {
    use crate::token::CueToken;
    use super::{Remark, RemarkType};

    #[test]
    fn parse() {
        let remark = Remark::from_line(CueToken::parse_line(r#"REM DATE "2022""#).as_slice());
        assert_eq!(remark, Remark{ remark_type: RemarkType::Date, text: "2022".into() });
        let rem_unk = Remark::from_line(CueToken::parse_line(r#"REM REPLAYGAIN_TRACK_GAIN -10.73 dB"#).as_slice());
        assert_eq!(rem_unk, Remark{ remark_type: RemarkType::Miscellaneous("REPLAYGAIN_TRACK_GAIN".into()), text: r#"-10.73 dB"#.into() });
    }

    #[test]
    fn display() {
        let remark = Remark::from_line(CueToken::parse_line(r#"REM COMMENT this is a comment"#).as_slice());
        assert_eq!(remark.to_string(), r#"REM COMMENT "this is a comment""#.to_string());
        let rem_unk = Remark::from_line(CueToken::parse_line(r#"REM REPLAYGAIN_TRACK_GAIN -10.73 dB"#).as_slice());
        assert_eq!(rem_unk.to_string(), r#"REM REPLAYGAIN_TRACK_GAIN "-10.73 dB""#);
    }

    // note maybe_write() is tested implicitly in Track tests
}
