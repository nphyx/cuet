use core::time::Duration;
/// General error while parsing cue point.
#[derive(Debug, Clone)]
pub struct CuePointParseError;

impl std::fmt::Display for CuePointParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "(parse error)")
    }
}


/// Represents a point in playback time.
///
/// From the format "MM:SS:FF" used in cuesheets:
///
/// - MM is minutes.
/// - SS is seconds.
/// - FF is frames (there are 75 frames in a second).
#[derive(Debug,Default,Clone,PartialEq,Eq,PartialOrd,Ord)]
pub struct CuePoint {
    pub minute: u16,
    pub second: u16,
    pub frame: u16,
}

impl<'a> CuePoint {
    /// derive from a point string of the format "MM:SS:FF"
    pub fn from_point_str(cue_point: &'a str) -> Result<Self, CuePointParseError> {
        let parts:Vec<&'a str> = cue_point.split(':').collect();
        if parts.len() != 3 {
            return Err(CuePointParseError);
        }
        let minute: u16 = parts.first().ok_or(CuePointParseError)?.parse::<u16>().map_err(|_| CuePointParseError)?;
        let second: u16 = parts.get(1).ok_or(CuePointParseError)?.parse::<u16>().map_err(|_| CuePointParseError)?;
        let frame: u16 = parts.get(2).ok_or(CuePointParseError)?.parse::<u16>().map_err(|_| CuePointParseError)?;
        Ok(Self {
            minute,
            second,
            frame,
        })
    }

    /// convert from a Duration to a CuePoint
    pub fn try_from_duration(dur: Duration) -> Result<Self, std::num::TryFromIntError> {
        let minute = u16::try_from(dur.as_secs() / 60)?;
        let second = u16::try_from(dur.as_secs().rem_euclid(60))?;
        let frame = u16::try_from(dur.subsec_millis() * 75 / 1000)?;
        Ok(Self { minute, second, frame })
    }

    /// convert to a duration with millisecond precision
    pub fn as_duration(&self) -> Duration {
        Duration::from_millis(u64::from(self.minute) * 60_000 + u64::from(self.second) * 1000 + (u64::from(self.frame) * 1000 / 75 ))
    }

    /// convert to seconds, frac pair (symphonia)
    pub fn as_time(&self) -> (u64, f64) {
        (u64::from(self.minute * 60 + self.second), f64::from(self.frame) / 75.0)
    }
}

impl std::fmt::Display for CuePoint {
    /// convert to a point string in the format "MM:SS:FF"
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:02}:{:02}:{:02}", self.minute, self.second, self.frame)
    }
}

#[cfg(test)]
mod test_cue_point {
    use std::time::Duration;
    use super::CuePoint;

    #[test]
    fn from_point_str() {
        let point = CuePoint::from_point_str("22:11:01");
        assert_eq!(point.unwrap(), CuePoint{ minute: 22, second: 11, frame: 1});
    }
    #[test]
    fn try_from_duration() {
        let dur = Duration::from_micros(65_333_333);
        assert_eq!(CuePoint::try_from_duration(dur), Ok(CuePoint { minute: 1, second: 5, frame: 24 }));
    }

    #[test]
    fn as_duration() {
        let point = CuePoint { minute: 5, second: 33, frame: 25 };
        let dur: Duration = point.as_duration();
        assert_eq!(dur.as_secs(), 333);
        assert_eq!(dur.subsec_millis(), 333);
    }

    #[test]
    fn display() {
        let cue_string = "02:11:45";
        assert_eq!(cue_string.to_string(), CuePoint::from_point_str(cue_string).unwrap_or_else(|_| Default::default()).to_string());
    }

    #[test]
    fn as_time() {
        let point = CuePoint::from_point_str("2:33:25").unwrap();
        assert_eq!(point.as_time(), (153, 1.0/3.0));
    }
}
