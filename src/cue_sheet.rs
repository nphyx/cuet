use std::collections::BTreeMap;
use indenter::indented;
use crate::util::{collect_texts, maybe_write};
use crate::token::{CueSymbol,CueToken};
use crate::track::{Track,TrackParser};
use crate::remark::{Remark,Remarks};
use crate::file::File;
use std::io::{BufRead,BufReader};

const UNKNOWN: &str = "UNKNOWN";

/// A complete representation of a cuesheet.
#[derive(Debug,Default,Clone,PartialEq,Eq)]
pub struct CueSheet {
    /// M-C-N catalog number
    pub catalog: Option<String>,
    /// associated files
    pub files: Vec<File>,
    /// non-standard cue detected (EAC, etc)
    pub nonstandard: bool,
    /// album's primary artist (tracks may have their own)
    pub performer: Option<String>,
    /// remark/comment, used to store additional arbitrary metadata
    pub remarks: Remarks,
    /// album's primary writer (tracks may have their own)
    pub songwriter: Option<String>,
    /// text file location
    pub text_file: Option<String>,
    /// album title
    pub title: Option<String>,
    /// ordered collection of tracks
    pub tracks: BTreeMap<usize, Track>,
    cur_file: Option<File>,
    cur_track: Option<TrackParser>,
    skipped_lines: Vec<usize>,
}

impl From<String> for CueSheet {
    /// parses a cuesheet string, consuming it in the process
    fn from(string: String) -> CueSheet {
        let mut sheet = CueSheet::default();
        sheet.parse_string(&string);
        sheet
    }
}

impl From<&String> for CueSheet {
    /// parses a cuesheet string
    fn from(string: &String) -> CueSheet {
        let mut sheet = CueSheet::default();
        sheet.parse_string(string);
        sheet
    }
}

impl From<BufReader<std::fs::File>> for CueSheet {
    /// parses a cuesheet from a BufReader
    fn from(buf: BufReader<std::fs::File>) -> CueSheet {
        let mut sheet = CueSheet::default();
        sheet.parse_file_buffer(buf);
        sheet
    }
}

impl std::fmt::Display for CueSheet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use std::fmt::Write;
        maybe_write(f, "TITLE", &self.title)?;
        maybe_write(f, "PERFORMER", &self.performer)?;
        maybe_write(f, "SONGWRITER", &self.songwriter)?;
        maybe_write(f, "CATALOG", &self.catalog)?;
        maybe_write(f, "CDTEXTFILE", &self.text_file)?;
        for remark in &self.remarks { remark.maybe_write(f, "")?; }
        for file in &self.files {
            writeln!(f, "{}", &file)?;
            for track_num in &file.tracks {
                if let Some(track) = self.tracks.get(track_num) {
                    write!(indented(f).with_str("  "), "{}", track)?;
                }
            }
        }

        Ok(())
    }
}

impl CueSheet {
    /// reset all properties of the sheet (useful for reusing the struct)
    pub fn wipe(&mut self) {
        self.catalog = None;
        self.files.clear();
        self.nonstandard = false;
        self.performer = None;
        self.remarks.clear();
        self.songwriter = None;
        self.text_file = None;
        self.title = None;
        self.tracks.clear();
    }

    /// wipes self and parses new properties from given string
    pub fn replace_from_string(&mut self, string: &str) {
        self.wipe();
        self.parse_string(string);
    }

    /// wipes self and parses new properties from given buffer
    pub fn replace_from_buffer(&mut self, buf: BufReader<std::fs::File>) {
        self.wipe();
        self.parse_file_buffer(buf);
    }

    fn take_line(&mut self, line: &[CueSymbol]) -> bool {
        use CueSymbol::{Command,Word,StringLiteral,Number};
        use CueToken as T;
        if let Some(track) = &mut self.cur_track {
            let ok = track.take_line(line);
            if ok {
                return true;
            }
        }
        match &line {
            [Command(T::Catalog), StringLiteral(n)] => {
                self.catalog = Some(n.clone());
                true
            },
            [Command(T::Catalog), Word(n)] => {
                self.catalog = Some(n.clone());
                true
            },
            [Command(T::Catalog), Number(n)] => {
                self.catalog = Some(n.to_string());
                true
            },
            [Command(T::Performer), ..] => {
                self.performer = Some(collect_texts(&line[1..]));
                true
            }
            [Command(T::CdTextFile), StringLiteral(file)] => {
                self.text_file = Some(file.clone());
                true
            },
            [Command(T::Rem), ..] => {
                self.remarks.push(Remark::from_line(line));
                true
            },
            [Command(T::Songwriter), ..] => {
                self.songwriter = Some(collect_texts(&line[1..]));
                true
            }
            [Command(T::Title), ..] => {
                self.title = Some(collect_texts(&line[1..]));
                true
            }
            [Command(T::Track), ..] => {
                if let Some(track) = self.cur_track.take() {
                    self.append_track(track.finish());
                }
                if let Some(parser) = TrackParser::from_line(line) {
                    self.cur_track = Some(parser);
                    true
                } else {
                    false
                }
            },
            [Command(T::File), ..] => {
                if let Some(file) = self.cur_file.take() {
                    self.append_file(file);
                }
                if let Some(file) = File::from_line(line) {
                    self.cur_file = Some(file);
                    true
                } else {
                    false
                }
            },
            _ => false,
        }
    }

    pub fn append_file(&mut self, file: File) {
        self.files.push(file);
    }

    /// appends a track to the track collection.
    ///
    /// Track.number will be used as its hash key.
    pub fn append_track(&mut self, track: Track) {
        if let Some(file) = &mut self.cur_file {
            file.tracks.push(track.number);
        }
        self.tracks.insert(track.number, track);
    }


    fn line_skipped(&mut self, num: usize) {
        self.skipped_lines.push(num);
        self.nonstandard = true;
    }

    fn finish(&mut self) {
        if let Some(track) = self.cur_track.take() {
            self.append_track(track.finish());
        }
        if let Some(file) = self.cur_file.take() {
            self.append_file(file);
        }
    }

    /// parse a cuesheet from a BufReader.
    fn parse_file_buffer(&mut self, reader: BufReader<std::fs::File>) {
        for (i, line) in reader.lines().enumerate() {
            match line {
                Ok(string) => {
                    let parsed = CueToken::parse_line(&string);
                    if !self.take_line(&parsed) {
                        self.line_skipped(i);
                    }
                },
                Err(_) => {
                    self.line_skipped(i);
                }
            }
        }
        self.finish()
    }

    /// parse a cuesheet from a &str.
    fn parse_string(&mut self, string: &str) {
        for (i, line) in string.lines().enumerate() {
            let parsed = CueToken::parse_line(line);
            if !self.take_line(&parsed) {
                self.line_skipped(i);
            }
        }
        self.finish()
    }

    /// get skipped line count
    pub fn get_skipped_count(&self) -> usize {
        self.skipped_lines.len()
    }

    /// get the track at the given number if it exists
    pub fn track_num(&self, num: usize) -> Option<&Track> {
        self.tracks.get(&num)
    }

    pub fn track_at(&self, dur: std::time::Duration) -> usize {
        use crate::cue_point::CuePoint;
        let mut last: Option<&Track> = None;
        if let Ok(time) = CuePoint::try_from_duration(dur) {
            for (_, track) in &self.tracks {
                if let Some(index) = track.indices.first() {
                    if index.point < time {
                        last = Some(track);
                    } else {
                        break;
                    }
                }
            }
            match last {
                Some(track) => return track.number,
                None => return 0,
            }
        }
        0
    }

    /// get the track immediately following num
    pub fn track_after(&self, num: usize) -> Option<&Track> {
        let mut last: usize = 0;
        for (_, track) in &self.tracks {
            if last == num {
                return Some(track);
            }
            last = track.number;
        }
        None
    }

    /// get the track immediately preceding num
    pub fn track_before(&self, num: usize) -> Option<&Track> {
        let mut last_track: Option<&Track> = None;
        for (_, track) in &self.tracks {
            if track.number >= num {
                continue;
            } else {
                last_track = Some(track);
            }
        }
        last_track
    }

    /// gets performer if it exists
    pub fn get_performer(&self) -> &str {
        match &self.performer {
            Some(perf) => perf.as_ref(),
            None => UNKNOWN,
        }
    }

    /// gets title if it exists
    pub fn get_title(&self) -> &str {
        match &self.title {
            Some(title) => title.as_ref(),
            None => UNKNOWN,
        }
    }

}

#[cfg(test)]
mod test_cue_sheet {
    use super::CueSheet;
    use crate::file::FileType;
    use crate::file::File;
    const CUE_TEXT: &str = r#"TITLE "Cockroach Soufflé"
PERFORMER "The WasteEaters"
SONGWRITER "Convergence Entity Metacluster 8a289b45-9514-492e-a697-01c99268e77f"
CATALOG "0999999999993"
REM GENRE "Psychedelic Carnage Hymnal"
REM DATE "2095"
REM DISCID "D999999999"
REM COMMENT "Not a real album, don't panic"
FILE "The WasteEaters - Cockroach Soufflé.flac" WAVE
  TRACK 01 AUDIO
    TITLE "Having Grandma for Lunch"
    PERFORMER "The WasteEaters"
    INDEX 01 00:00:00
  TRACK 02 AUDIO
    TITLE "Victor's Wake"
    PERFORMER "The WasteEaters"
    INDEX 00 02:47:74
    INDEX 01 02:48:27
  TRACK 03 AUDIO
    TITLE "Iron Knee (My Second-Hand Cyberware is Killing Me)"
    PERFORMER "The WasteEaters"
    INDEX 00 05:41:50
    INDEX 01 05:42:27
  TRACK 04 AUDIO
    TITLE "Crater Pertaters, Boiled With Salt"
    PERFORMER "The WasteEaters"
    INDEX 00 08:53:47
    INDEX 01 08:54:37
  TRACK 05 AUDIO
    TITLE "I Was Told There Would Be Jetpacks"
    PERFORMER "The WasteEaters"
    INDEX 00 10:59:20
    INDEX 01 11:00:17
  TRACK 06 AUDIO
    TITLE "Push the Button Again (It Didn't Work the First Time)"
    PERFORMER "The WasteEaters"
    INDEX 00 13:20:55
    INDEX 01 13:20:67
"#;

    #[test]
    fn from_string() {
        use crate::remark::Remark;
        use crate::remark::RemarkType::*;
        let sheet = CueSheet::from(CUE_TEXT.to_string());
        assert_eq!(sheet.performer, Some("The WasteEaters".into()));
        assert_eq!(sheet.title, Some("Cockroach Soufflé".into()));
        assert_eq!(sheet.songwriter, Some("Convergence Entity Metacluster 8a289b45-9514-492e-a697-01c99268e77f".into()));
        assert_eq!(sheet.catalog, Some("0999999999993".into()));
        assert_eq!(sheet.remarks, vec![
                  Remark{ remark_type: Genre, text: "Psychedelic Carnage Hymnal".into() },
                  Remark{ remark_type: Date, text: "2095".into() },
                  Remark{ remark_type: DiscId, text: "D999999999".into() },
                  Remark{ remark_type: Comment, text: "Not a real album, don't panic".into() },
        ]);
        assert_eq!(sheet.tracks.len(), 6);
        // track parsing should already be tested so just one quick sanity check
        assert_eq!(sheet.tracks.get(&2).unwrap().title, Some("Victor's Wake".into()));
        assert_eq!(sheet.files.get(0).unwrap(), &File {
            name: "The WasteEaters - Cockroach Soufflé.flac".into(),
            file_type: FileType::Wave,
            tracks: vec![1,2,3,4,5,6],
        });
    }

    #[test]
    // output should be the same as input since CUE_TEXT is ideally formatted
    fn display() {
        let sheet = CueSheet::from(CUE_TEXT.to_string());
        assert_eq!(CUE_TEXT, sheet.to_string());
    }

    #[test]
    fn replace_from_string() {
        let mut sheet: CueSheet = Default::default();
        sheet.replace_from_string(CUE_TEXT);
        assert_eq!(CUE_TEXT, sheet.to_string());
    }

    #[test]
    fn from_buffer() -> Result<(), std::io::Error> {
        use std::path::Path;
        use std::fs::File;
        use std::io::BufReader;
        let d = &Path::new(env!("CARGO_MANIFEST_DIR"))
            .join("resources/test/hydrogenaudio_example.cue");
        assert!(d.is_file());
        let file = File::open(d)?;
        let buf_reader = BufReader::new(file);
        let sheet = CueSheet::from(buf_reader);
        assert_eq!(sheet.files.get(0).unwrap().name, "The Specials - Singles.wav");
        assert_eq!(sheet.files.get(0).unwrap().tracks.len(), 16);
        assert_eq!(sheet.tracks.get(&15).unwrap().title, Some("Nelson Mandela".into()));
        assert_eq!(sheet.tracks.len(), 16);
        Ok(())
    }
}
