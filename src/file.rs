use crate::token::{CueSymbol,CueToken};
use crate::util::collect_texts;

/// Recognized file types.
#[derive(Debug,Default,Clone,PartialEq,Eq)]
pub enum FileType {
    Wave,
    Mp3,
    Aiff,
    Binary,
    Motorola,
    Miscellaneous(String),
    #[default]
    Unknown,
}

impl From<CueToken> for FileType {
    fn from(token: CueToken) -> Self {
        match token {
            CueToken::FileWave => Self::Wave,
            CueToken::FileMp3 => Self::Mp3,
            CueToken::FileAiff => Self::Aiff,
            CueToken::FileBinary => Self::Binary,
            CueToken::FileMotorola => Self::Motorola,
            _ => Self::Unknown,
        }
    }
}

impl std::fmt::Display for FileType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use FileType::*;
        match self {
            Wave => write!(f, "WAVE"),
            Mp3 => write!(f, "MP3"),
            Aiff => write!(f, "AIFF"),
            Binary => write!(f, "BINARY"),
            Motorola => write!(f, "MOTOROLA"),
            Unknown => write!(f, "UNKNOWN"),
            Miscellaneous(s) => write!(f, "{}", s),
        }
    }
}

/// Representation of a file entry.
///
/// Note that all tracks are included in CueSheet::tracks; a file entry only contains a list of
/// keys for tracks associated with the file.
#[derive(Default,Debug,Clone,PartialEq,Eq)]
pub struct File {
    pub name: String,
    pub file_type: FileType,
    pub tracks: Vec<usize>,
}

impl File {
    pub fn new(file_type: FileType, name: String) -> Self {
        Self{ file_type, name, ..Default::default() }
    }

    pub fn from_line(line: &[CueSymbol]) -> Option<File> {
        match line {
            [CueSymbol::Command(_), .., CueSymbol::FileType(t)] => Some(File {
                name: collect_texts(&line[1..line.len() -1]),
                file_type: t.clone(),
                ..Default::default()
            }),
            [CueSymbol::Command(_), .., CueSymbol::Word(s)] => Some(File {
                name: collect_texts(&line[1..line.len() -1]),
                file_type: FileType::Miscellaneous(s.clone()),
                ..Default::default()
            }),
            _ => None
        }
    }
}

impl std::fmt::Display for File {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, r#"FILE "{}" {}"#, self.name, self.file_type)?;
        Ok(())
    }
}

#[cfg(test)]
mod test_file {
    use crate::token::CueToken;
    use super::*;

    #[test]
    fn from_line() {
        let unknown_type_line = CueToken::parse_line(r#"FILE "Unknown Album.wav" ASDF"#);
        assert_eq!(File::from_line(&unknown_type_line).unwrap(), File{
            file_type: FileType::Miscellaneous("ASDF".into()),
            name: "Unknown Album.wav".into(),
            ..Default::default()
        });
        let mp3_line = CueToken::parse_line(r#"FILE "My Album.mp3" MP3"#);
        assert_eq!(File::from_line(&mp3_line).unwrap(), File{
            file_type: FileType::Mp3,
            name: "My Album.mp3".into(),
            ..Default::default()
        });
        let wav_line = CueToken::parse_line(r#"FILE "My Album.flac" WAVE"#);
        assert_eq!(File::from_line(&wav_line).unwrap(), File{
            file_type: FileType::Wave,
            name: "My Album.flac".into(),
            ..Default::default()
        });
    }

    #[test]
    fn display() {
        let file = File::from_line(&CueToken::parse_line(r#"FILE "Unknown Album.wav" WAVE"#)).unwrap();
        assert_eq!(r#"FILE "Unknown Album.wav" WAVE"#, file.to_string());
    }
}
