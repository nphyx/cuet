use crate::token::CueSymbol;

pub fn dequote(string: String) -> String {
    if string.starts_with('"') && string.ends_with('"') {
        return string[1..string.len() - 1].into();
    }
    string
}

pub fn quote(string: String) -> String {
    if string.starts_with('"') && string.ends_with('"') {
        return string;
    }
    let mut constructed = String::from("\"");
    constructed.push_str(&string);
    constructed.push('"');
    constructed
}

pub fn collect_texts(items: &[CueSymbol]) -> String {
    let mut collected: Vec<String> = Vec::new();
    for item in items.iter() {
        match item {
            CueSymbol::StringLiteral(text) => {
                collected.push(quote(text.clone()));
            },
            CueSymbol::Word(text) => {
                collected.push(text.clone());
            },
            CueSymbol::Number(n) => {
                collected.push(n.to_string());
            }
            _ => {},
        }
    }
    // clean up surrounding quotes
    dequote(collected.join(" "))
}

/// write an Option<String> to formatter only if exists and not empty
pub fn maybe_write(f: &mut std::fmt::Formatter<'_>, pre: &str, val: &Option<String>) -> std::fmt::Result {
    if let Some(v) = val {
        if !v.is_empty() {
            return writeln!(f, "{} \"{}\"", pre, v);
        }
    }
    Ok(())
}

#[cfg(test)]
mod test_util {
    use crate::token::{CueToken::*, CueSymbol as S};
    use super::*;
    #[test]
    fn collect() {
        let collected = collect_texts(&[S::Command(Rem), S::word("foo"), S::string("bar".into()), S::word("baz")]);
        assert_eq!(collected, r#"foo "bar" baz"#);
    }
}

