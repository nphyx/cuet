Cuet
====

A Rust-native, reasonably complete and extremely fast parser for cuesheets.

Cuet's API is **NOT** stable, but existing methods shouldn't change much.

See [the HydrogenAudio wiki entry](https://wiki.hydrogenaud.io/index.php?title=Cue_sheet) and the [CDRWIN user guide](https://web.archive.org/web/20070221154246/http://www.goldenhawk.com/download/cdrwin.pdf).

Features
--------

- Rust-native - no jank C FFI.
- Very fast - parsing a cuesheet from a file or buffer takes nanoseconds at most.
- Supports all documented commands and exposes all properties in a cuesheet, including arbitrary remarks, extended metadata, pre- and post-gaps, catalog numbers, etc.
- May not fully support all non-standard extensions to the cuesheet format, but it should handle them sanely.
- Outputs parsed cuesheets in a cleaned-up, valid, but otherwise identical format to the one it parsed.
- Very few dependencies and minimal bloat.

Example
-------

Cuet does not provide facilities for reading or writing files, whether cuesheet text files or as in embedded audio metadata.

It parses cuesheets efficiently from either a `BufReader` or a `String`, and can output them as valid, properly formatted cuesheet texts.

Parse from a file:

```rust
use std::fs::File;
use std::io::BufReader;
use cuet::CueSheet;

let cuesheet = CueSheet::from(
	BufReader::new(File::open("resources/test/hydrogenaudio_example.cue")?)
);
assert_eq!(cuesheet.title.unwrap(), "Singles");
assert_eq!(cuesheet.performer.unwrap(), "The Specials");
assert_eq!(cuesheet.tracks.get(&1).unwrap().title, Some(String::from("Gangsters")));
// etc.
# Ok::<(), std::io::Error>(())
```

Parse from a string:

```rust
use cuet::CueSheet;

let cuesheet_string = String::from(r#"TITLE "My Album Title""#);
/// etc. commands

let cuesheet = CueSheet::from(&cuesheet_string);
assert_eq!(cuesheet.title.unwrap(), "My Album Title");
// etc.
```

Emit a cuesheet from a parsed cuesheet:

```rust
use cuet::CueSheet;

let cuesheet_string = String::from(r#"TITLE "My Album Title"
"#);

let cuesheet = CueSheet::from(&cuesheet_string);
assert_eq!(cuesheet_string, cuesheet.to_string()); // should be identical!
```

Make your own cuesheet:

```rust
use cuet::*;

let mut cuesheet = CueSheet::default();

cuesheet.title = Some(String::from("My Album Name"));
cuesheet.performer = Some(String::from("My Band Name"));
let mut file = File::new(
    FileType::Wave,
    String::from("My Album Name - My Band Name.wav"),
);
file.tracks.push(1);
cuesheet.append_file(file);
let mut track = Track::new(TrackType::Audio);
track.number = 1;
track.title = Some(String::from("Track 1"));
track.append_index_str(0, "00:00:00");
track.append_index_str(1, "00:03:00");
track.append_remark(RemarkType::Comment, String::from("The first track!"));
cuesheet.append_track(track);
assert_eq!(cuesheet.to_string(), r#"TITLE "My Album Name"
PERFORMER "My Band Name"
FILE "My Album Name - My Band Name.wav" WAVE
  TRACK 01 AUDIO
    TITLE "Track 1"
    REM COMMENT "The first track!"
    INDEX 00 00:00:00
    INDEX 01 00:03:00
"#
);
# Ok::<(), cuet::CuePointParseError>(())
```

Tips
----

[rust-metaflac](https://github.com/jameshurst/rust-metaflac) is a useful library for extracting cue sheets from flac files.

Bugs
----

Please [file a bug report](https://gitlab.com/nphyx/cuet/-/issues) if you run into any issues. If possible, include the cuesheet that caused the problem along with a description of what it did and what you expected it to do instead.

Please do **NOT** attach any proprietary audio files with embedded cues - just extract the cuesheet and attach it.

License
-------

[MIT](LICENSE)
